#include <iostream>
#include "document.h"
#include "person.h"
#include "date.h"
#include "library.h"
#include "borrow.h"
#include<exception>
struct test{
	int a;
};
 int main(){
	 /*
	 test obj[2];
	 obj[0].a = 7;
	 obj[1].a = 9;
	 try{
		FileCls<test> file("test.bin",2,obj);
		file.writeToFile();
	}catch(const char *err){
		std::cout<<err<<std::endl;
	}
	test *re;
	FileCls<test> fi("test.bin");
	int n;
	fi.readFromFile(n,&re);
	std::cout<<re[0].a<<std::endl;
	std::cout<<re[1].a<<std::endl;*/
	int sw=1;
	Library a;
	Borrow b;
 	while(sw){
		std::cout<<"1-Add 2-Show 3-Delete 4-Borrow 5-Edit 6-saveToFile 7-loadFromFile;"<<std::endl<<"Enter Option = ";
		std::cin>>sw;
		switch(sw){
			case 1 :
				a.input_library();
				break;
			case 2:
				a.output_library();
				break;
			case 3:
				a.delete_library();
				break;
			case 4:
				b.main_borrow(a);
				break;
			case 5:
				a.edit_library();
				break;
			case 6:
				b.saveToFile();
				a.saveToFile();
				break;
			case 7:
				try{
					b.loadFromFile();
					a.loadFromFile();
				}catch(std::exception err){
					std::cout<<err.what()<<std::endl;
				}
				break;
			case 0:
				exit(0);
				break;
			}


	}
	return 0;
}
