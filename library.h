#pragma once
#include <iostream>
#include <vector>
#include <thread>
#include "person.h"
#include "document.h"
#include "file.cpp"

class Library{
	public:
	Book *book;
	Mag *mag;
	News_Paper *news_paper;
	Librarian *librarian;
	Member *member;
	Worker *worker;
	Writer *writer;
	int num[7];
	//book_num=0,mag_num=1,news_paper_num=2,librarian_num=3,member_num=4,worker_num=5,write_num=6;
	
		/*
		void addBook();
		void addMag();
		void addNewsPaper();
		void addLibrarian();
		void addMember();
		void addWorker();
		void addWriter();

		void displayBooks();
		void displayMags();
		void displayNewsPapers();
		void displayLibrarians();
		void displayMembers();
		void displayWorkers();
		void displayWriters();
		* 
		* be ja ina mishe az tabe ziram estefade kard
		<template item>
		 void display(item *a,int amount){
			for(int i =0;i<amount;i++)
			 	std::cout<<item[i]<<std::endl;
		 }


		void editBook(int);
		void editMag(int);
		void editNewsPaper(int);
		void editLibrarian(int);
		void editMember(int);
		void editWorker(int);
		void editWriter(int);

		void deleteBook(int);
		void deleteMag(int);
		void deleteNewsPaper(int);
		void deleteLibrarian(int);
		void deleteMember(int);
		void deleteWorker(int);
		void deleteWriter(int);
		
		*/
	Library(){
		for(int i =0;i<7;i++)
			num[i]=0;
		book = 0;
		mag = 0;
		news_paper = 0;
		librarian = 0;
		member = 0;
		worker = 0;
		writer = 0;
	}
	void search_library();
	void input_library();
	void output_library();
	void edit_library();
	void delete_library();
	/*Library(){
		int a=0;
		while(a<7){
			num[a]=0;
			a=a+1;
		}
	}*/
	template <class T>
	friend void input_onest(Library& a,T **b,int n);
	template <class TI>
	friend void delete_onest(Library& a,TI *b,int n,int id);
	template <class V>
	friend void writeData(const char * filename,V *data,int i);
	void saveToFile();
	void loadFromFile();
};
