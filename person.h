#include<iostream> 
#include "date.h"
#pragma once
class Person{
	protected :
	//public:
		char name[20];
		char lname[20];

		DateCls birth;
		public :
		int idNumber;
		virtual void addPerson()= 0;
		virtual void showPerson() = 0;

};
class Librarian : public Person{
	private:
		int employNumber;
		char sectionName[30];
		char employYear[4];
	public:
		void addPerson();
		void showPerson();
	    friend std::ostream &operator<<( std::ostream &output,const Librarian &D);
	    friend std::istream &operator>>( std::istream &input, Librarian &D);
		      
};

class Member : public Person{
	private:
		int memberId;
		int registerYear;
	public:
		void addPerson();
		void showPerson();
		friend std::ostream &operator<<( std::ostream &output,const Member &D);
		friend std::istream &operator>>( std::istream &input,Member &D);
};
class Worker : public Person{
	private:
		int employNumber;
		char employYear[4];
	public:
		void addPerson();
		void showPerson();
		friend std::ostream &operator<<( std::ostream &output,const Worker &D);
		friend std::istream &operator>>( std::istream &input,Worker &D);
};
class Writer : public Person{
	private:
		int writerId;
	public:
		void addPerson();
		void showPerson();
		friend std::ostream &operator<<( std::ostream &output,const Writer &D);
		friend std::istream &operator>>( std::istream &input,Writer &D);
};
