#include "person.h"


	
void Librarian::addPerson(){
	
	//person
	std::cout<<"-----new Person -----"<<std::endl;
	std::cout<<"Enter Name "<<std::endl; 
	std::cin>>name;
	std::cout<<"Enter LastName "<<std::endl;
	std::cin>>lname;
	std::cout<<"Enter Id Number"<<std::endl;
	std::cin>>idNumber;
	std::cout<<"Enter Birth Date"<<std::endl;
	std::cin>>birth.day;
	std::cin>>birth.month;
	std::cin>>birth.year;
	//librarian
	
	std::cout<<"Enter Employ Number"<<std::endl;
	std::cin>>employNumber;
	std::cout<<"Enter Section Name "<<std::endl;
	std::cin>>sectionName;
	std::cout<<"Enter Employ Year"<<std::endl;
	std::cin>>employYear;
	
}

void Librarian::showPerson(){
	
	//person
	std::cout<<"----- Person -----"<<std::endl;
	std::cout<<"Name : " ;
	std::cout<<name<<std::endl;
	std::cout<<"LastName : ";
	std::cout<<lname<<std::endl;
	std::cout<<"Id Number : ";
	std::cout<<idNumber<<std::endl;
	std::cout<<"Birth Date : ";
	std::cout<<birth.day<<" / "<<birth.month << " / "<<birth.year<<std::endl;

	//librarian	
	std::cout<<"Employ Number : ";
	std::cout<<employNumber<<std::endl;
	std::cout<<"Section Name : ";
	std::cout<<sectionName<<std::endl;
	std::cout<<"Employ Year : ";
	std::cout<<employYear;
}

std::ostream &operator<<( std::ostream &output,const Librarian &D){
	//output<<"----- Person -----"<<std::endl<<"Name : " ;
		//person
	output<<"----- Person -----"<<std::endl;
	output<<"Name : " ;
	output<<D.name<<std::endl;
	output<<"LastName : ";
	output<<D.lname<<std::endl;
	output<<"Id Number : ";
	output<<D.idNumber<<std::endl;
	output<<"Birth Date : ";
	output<<D.birth.day<<" / "<<D.birth.month << " / "<<D.birth.year<<std::endl;

	//librarian	
	output<<"Employ Number : ";
	output<<D.employNumber<<std::endl;
	output<<"Section Name : ";
	output<<D.sectionName<<std::endl;
	output<<"Employ Year : ";
	output<<D.employYear;
	return output;
}
//friend std::istream &operator>>( std::istream  &input, Librarian &D )
std::istream &operator>>( std::istream &input,Librarian &D){
	std::cout<<"-----Librarian -----"<<std::endl;
	std::cout<<"Enter Name "<<std::endl; 
	input>>D.name;
	std::cout<<"Enter LastName "<<std::endl;
	input>>D.lname;
	std::cout<<"Enter Id Number"<<std::endl;
	input>>D.idNumber;
	std::cout<<"Enter Birth Date"<<std::endl;
	input>>D.birth.day;
	input>>D.birth.month;
	input>>D.birth.year;
	//librarian
	
	std::cout<<"Enter Employ Number"<<std::endl;
	input>>D.employNumber;
	std::cout<<"Enter Section Name "<<std::endl;
	input>>D.sectionName;
	std::cout<<"Enter Employ Year"<<std::endl;
	input>>D.employYear;
	return input;
}

void Member::addPerson(){
	
	//person
	std::cout<<"-----new Person -----"<<std::endl;
	std::cout<<"Enter Name "<<std::endl; 
	std::cin>>name;
	std::cout<<"Enter LastName "<<std::endl;
	std::cin>>lname;
	std::cout<<"Enter Id Number"<<std::endl;
	std::cin>>idNumber;
	std::cout<<"Enter Birth Date"<<std::endl;
	std::cin>>birth.day;
	std::cin>>birth.month;
	std::cin>>birth.year;
	//Member
	
	std::cout<<"Enter MemberId "<<std::endl;
	std::cin>>memberId;
	std::cout<<"Enter Register Year "<<std::endl;
	std::cin>>registerYear;
	
}

void Member::showPerson(){
	
	//person
	std::cout<<"----- Person -----"<<std::endl;
	std::cout<<"Name : " ;
	std::cout<<name<<std::endl;
	std::cout<<"LastName : ";
	std::cout<<lname<<std::endl;
	std::cout<<"Id Number : ";
	std::cout<<idNumber<<std::endl;
	std::cout<<"Birth Date : ";
	std::cout<<birth.day<<" / "<<birth.month << " / "<<birth.year<<std::endl;

	//Member
	std::cout<<"Member Id : ";
	std::cout<<memberId<<std::endl;
	std::cout<<"Register Year : ";
	std::cout<<registerYear<<std::endl;
}
std::ostream &operator<<( std::ostream &output,const Member &D){
	//output<<"----- Person -----"<<std::endl<<"Name : " ;
		//person
	output<<"----- Person -----"<<std::endl;
	output<<"Name : " ;
	output<<D.name<<std::endl;
	output<<"LastName : ";
	output<<D.lname<<std::endl;
	output<<"Id Number : ";
	output<<D.idNumber<<std::endl;
	output<<"Birth Date : ";
	output<<D.birth.day<<" / "<<D.birth.month << " / "<<D.birth.year<<std::endl;

	//Member
	output<<"Member Id : ";
	output<<D.memberId<<std::endl;
	output<<"Register Year : ";
	output<<D.registerYear<<std::endl;
	return output;
}
std::istream &operator>>( std::istream &input,Member &D){
	
	//person
	std::cout<<"-----Member -----"<<std::endl;
	std::cout<<"Enter Name "<<std::endl; 
	input>>D.name;
	std::cout<<"Enter LastName "<<std::endl;
	input>>D.lname;
	std::cout<<"Enter Id Number"<<std::endl;
	input>>D.idNumber;
	std::cout<<"Enter Birth Date"<<std::endl;
	input>>D.birth.day;
	input>>D.birth.month;
	input>>D.birth.year;
	//Member
	
	std::cout<<"Enter MemberId "<<std::endl;
	input>>D.memberId;
	std::cout<<"Enter Register Year "<<std::endl;
	input>>D.registerYear;
	return input;
}


void Worker::addPerson(){
	
	//person
	std::cout<<"-----new Person -----"<<std::endl;
	std::cout<<"Enter Name "<<std::endl; 
	std::cin>>name;
	std::cout<<"Enter LastName "<<std::endl;
	std::cin>>lname;
	std::cout<<"Enter Id Number"<<std::endl;
	std::cin>>idNumber;
	std::cout<<"Enter Birth Date"<<std::endl;
	std::cin>>birth.day;
	std::cin>>birth.month;
	std::cin>>birth.year;
	//Worker
	
	std::cout<<"Enter Employ Number"<<std::endl;
	std::cin>>employNumber;
	std::cout<<"Enter Employ Year"<<std::endl;
	std::cin>>employYear;
	
}

void Worker::showPerson(){
	
	//person
	std::cout<<"----- Person -----"<<std::endl;
	std::cout<<"Name : " ;
	std::cout<<name<<std::endl;
	std::cout<<"LastName : ";
	std::cout<<lname<<std::endl;
	std::cout<<"Id Number : ";
	std::cout<<idNumber<<std::endl;
	std::cout<<"Birth Date : ";
	std::cout<<birth.day<<" / "<<birth.month << " / "<<birth.year<<std::endl;

	//Worker
	std::cout<<"Employ Number : ";
	std::cout<<employNumber<<std::endl;
	std::cout<<"Employ Year : ";
	std::cout<<employYear;
}

std::ostream &operator<<( std::ostream &output,const Worker &D){
	//output<<"----- Person -----"<<std::endl<<"Name : " ;
		//person
	output<<"----- Person -----"<<std::endl;
	output<<"Name : " ;
	output<<D.name<<std::endl;
	output<<"LastName : ";
	output<<D.lname<<std::endl;
	output<<"Id Number : ";
	output<<D.idNumber<<std::endl;
	output<<"Birth Date : ";
	output<<D.birth.day<<" / "<<D.birth.month << " / "<<D.birth.year<<std::endl;

	//Woker
	output<<"Employ Number : ";
	output<<D.employNumber<<std::endl;
	output<<"Employ Year : ";
	output<<D.employYear;
	return output;
}
std::istream &operator>>( std::istream &input,Worker &D){
		
	//person
	std::cout<<"-----Worker-----"<<std::endl;
	std::cout<<"Enter Name "<<std::endl; 
	input>>D.name;
	std::cout<<"Enter LastName "<<std::endl;
	input>>D.lname;
	std::cout<<"Enter Id Number"<<std::endl;
	input>>D.idNumber;
	std::cout<<"Enter Birth Date"<<std::endl;
	input>>D.birth.day;
	input>>D.birth.month;
	input>>D.birth.year;
	//Worker
	
	std::cout<<"Enter Employ Number"<<std::endl;
	input>>D.employNumber;
	std::cout<<"Enter Employ Year"<<std::endl;
	input>>D.employYear;
	return input;
}

void Writer::addPerson(){
	
	//person
	std::cout<<"-----new Person -----"<<std::endl;
	std::cout<<"Enter Name "<<std::endl; 
	std::cin>>name;
	std::cout<<"Enter LastName "<<std::endl;
	std::cin>>lname;
	std::cout<<"Enter Id Number"<<std::endl;
	std::cin>>idNumber;
	std::cout<<"Enter Birth Date"<<std::endl;
	std::cin>>birth.day;
	std::cin>>birth.month;
	std::cin>>birth.year;
	//Writer
	
	std::cout<<"Enter Writer Id"<<std::endl;
	std::cin>>writerId;

	
}

void Writer::showPerson(){
	
	//person
	std::cout<<"----- Person -----"<<std::endl;
	std::cout<<"Name : " ;
	std::cout<<name<<std::endl;
	std::cout<<"LastName : ";
	std::cout<<lname<<std::endl;
	std::cout<<"Id Number : ";
	std::cout<<idNumber<<std::endl;
	std::cout<<"Birth Date : ";
	std::cout<<birth.day<<" / "<<birth.month << " / "<<birth.year<<std::endl;
	//Writer	
	std::cout<<"Writer Id : ";
	std::cout<<writerId<<std::endl;
}
std::ostream &operator<<( std::ostream &output,const Writer &D){
	//output<<"----- Person -----"<<std::endl<<"Name : " ;
		//person
	output<<"----- Person -----"<<std::endl;
	output<<"Name : " ;
	output<<D.name<<std::endl;
	output<<"LastName : ";
	output<<D.lname<<std::endl;
	output<<"Id Number : ";
	output<<D.idNumber<<std::endl;
	output<<"Birth Date : ";
	output<<D.birth.day<<" / "<<D.birth.month << " / "<<D.birth.year<<std::endl;

	//Writer
	output<<"Writer Id : ";
	output<<D.writerId<<std::endl;
	return output;
}
std::istream &operator>>( std::istream &input,Writer &D){
		//person
	std::cout<<"-----new Writer -----"<<std::endl;
	std::cout<<"Enter Name "<<std::endl; 
	input>>D.name;
	std::cout<<"Enter LastName "<<std::endl;
	input>>D.lname;
	std::cout<<"Enter Id Number"<<std::endl;
	input>>D.idNumber;
	std::cout<<"Enter Birth Date"<<std::endl;
	input>>D.birth.day;
	input>>D.birth.month;
	input>>D.birth.year;
	//Writer
	
	std::cout<<"Enter Writer Id"<<std::endl;
	input>>D.writerId;
	return input;
}
