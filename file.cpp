#include<iostream>
#include <fstream>
#include "exception.h"
#pragma once

template <class T>
class FileCls{
	private:
		std::fstream f1;
		int n;
		T *data;
	public:
		FileCls(const char * filename,int n,T *ar){
			f1.open(filename,std::ios::out|std::ios::binary);
			if(!f1.is_open())throw fileExc(0);
			data = ar;
			this->n = n;
			
		}
		
		FileCls(const char * filename){
			f1.open(filename,std::ios::in|std::ios::binary);
			if(!f1.is_open())throw fileExc(0);
			
		}
		void writeToFile(){
				f1.write((char*)&n,sizeof(int));
				f1.write((char*)data,sizeof(T)*n);
		}
		
		void readFromFile(int &n,T **data){
			f1.read((char*)&n,sizeof(int));
			*data = new T[n];
			f1.read((char*)(*data),sizeof(T)*n);
			//n = infile.read (buffer,size);
		}

		void close(){
			f1.close();
		}
		~FileCls(){
			f1.close();
		}
	
	
};

