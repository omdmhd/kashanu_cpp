#include "library.h"
#include<exception>
template <class T>
void input_onest(Library& a,T **b,int n){

	int i=0;
	T *temp;
	temp = new T[a.num[n]+1];
	while(i<a.num[n]){
		temp[i]=b[0][i];
		//std::cout<<temp[i]<<std::endl;
		//std::cout<<*b[i]<<std::endl;
		i=i+1;
	}
	std::cin >> temp[a.num[n]];
	delete[] *b;
	std::cout<<"Address before delete "<< *b <<std::endl;
	b[0] = temp;
	std::cout<<*b<<"  "<<temp<<std::endl;
	a.num[n]++;
}
template <class TI>
void delete_onest(Library& a,TI *b,int n,int id){
	while(id<a.num[n]){
		b[id]=b[id+1];
		id++;
	}
	a.num[n]--;
}
int isequal(char *a,char *b){
	int i=0,flag=1;
	while(a[i]!=0 && flag==1){
		if(a[i]!=b[i]){
			flag = 0;
		}
		i++;
	}
	return flag;
}
template <class Ty>
void search(Library& a,Ty *b,int n){
	char *name;
	name = new char[30];
	std::cout << "enter the key word to search" << std::endl;
	std::cin >> name;
	int i=0,flag=0;
	while(i<a.num[n]){
		if(isequal(name,b[i].name)){
			flag = 1;
			std::cout << b[i];std::cout << std::endl;
		}
		i++;
	}
	if(flag==0){
		std::cout << "not found" << std::endl;
	}
}
template <class V>
void writeData(const char * filename,V *data,int i){
	FileCls<V> file(filename,i,data);
	file.writeToFile();	
}
void Library::delete_library(){
	int sw2 = 1;
	while(sw2){
		int a = 0;
		int id;
		bool flagC;
		std::cout<<"1-Delete Book 2-Delete Mag 3-Delete News Paper 4-Delete Librarian 5-Delete Member 6-Delete Worker 7-Delete Writer  0-Exit;"<<std::endl<<"Enter Option = ";
		std::cin>>sw2;
		switch(sw2){
			case 1 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(book[a].id == id){
						delete_onest<Book>(*(this),book,sw2-1,a);
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 2 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(mag[a].id == id){
						delete_onest<Mag>(*(this),mag,sw2-1,a);
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}				break;
			case 3 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(news_paper[a].id == id){
						delete_onest<News_Paper>(*(this),news_paper,sw2-1,a);
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 4 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(librarian[a].idNumber == id){
						delete_onest<Librarian>(*(this),librarian,sw2-1,a);
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 5 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(member[a].idNumber == id){
						delete_onest<Member>(*(this),member,sw2-1,a);
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 6 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(worker[a].idNumber == id){
						delete_onest<Worker>(*(this),worker,sw2-1,a);
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 7 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(writer[a].idNumber == id){
						delete_onest<Writer>(*(this),writer,sw2-1,a);
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 0 :
				break;
		}
	}	
}
void Library::input_library(){
	int sw1=1;
	while(sw1){
		std::cout<<"1-Add Book 2-Add Mag 3-Add News Paper 4-Add Librarian 5-Add Member 6-Add Worker 7-Add Writer 8-Save To File 0-Exit;"<<std::endl<<"Enter Option = ";
		std::cin>>sw1;
		switch(sw1){
		case 1 :
			input_onest<Book>(*(this),&book,sw1-1);
			break;
		case 2 :
			input_onest<Mag>(*(this),&mag,sw1-1);
			break;
		case 3 :
			input_onest<News_Paper>(*(this),&news_paper,sw1-1);
			break;
		case 4 :
			input_onest<Librarian>(*(this),&librarian,sw1-1);
			break;
		case 5 :
			input_onest<Member>(*(this),&member,sw1-1);
			break;
		case 6 :
			input_onest<Worker>(*(this),&worker,sw1-1);
			break;
		case 7 :
			input_onest<Writer>(*(this),&writer,sw1-1);
			break;
		case 8:
						try{
				saveToFile();
			}catch(std::exception err){
				std::cout<<err.what()<<std::endl;
			}
		case 0 :
			break;
		}
	}
}
void Library::output_library(){
	int sw2 = 1;
	while(sw2){
		int a = 0;
		std::cout<<"1-Show Book 2-Show Mag 3-Show News Paper 4-Show Librarian 5-Show Member 6-Show Worker 7-Show Writer 8-Load From File 0-Exit;"<<std::endl<<"Enter Option = ";
		std::cin>>sw2;
		switch(sw2){
			case 1 :
				a=0;
				std::cout<<"num is "<<num[sw2-1]<<""<<std::endl;
				while(a<num[sw2-1]){			
					std::cout << book[a];std::cout << "-----------------------------------"<<std::endl;
					a=a+1;
				}
				break;
			case 2 :
				a=0;
				while(a<num[sw2-1]){
					std::cout << mag[a];std::cout << "-----------------------------------"<<std::endl;
					a=a+1;
				}
				break;
			case 3 :
				a=0;
				while(a<num[sw2-1]){
					std::cout << news_paper[a];std::cout << "-----------------------------------"<<std::endl;
					a=a+1;
				}
				break;
			case 4 :
				a=0;
				while(a<num[sw2-1]){
					std::cout << librarian[a];std::cout << "-----------------------------------"<<std::endl;
					a=a+1;
				}
				break;
			case 5 :
				a=0;
				while(a<num[sw2-1]){
					std::cout << member[a];std::cout << "-----------------------------------"<<std::endl;
					a=a+1;
				}
				break;
			case 6 :
				a=0;
				while(a<num[sw2-1]){
					std::cout << worker[a];std::cout << "-----------------------------------"<<std::endl;
					a=a+1;
				}
				break;
			case 7 :
				a=0;
				while(a<num[sw2-1]){
					std::cout << writer[a];std::cout << "-----------------------------------"<<std::endl;
					a=a+1;
				}
				break;
			case 8:
				loadFromFile();
				break;
			case 0 :
				break;
		}
	}	
}
void Library::search_library(){
	int sw = 1;
	while(sw){
		std::cout<<"1-Search Book 2-Search Mag 3-Search News Paper"<<std::endl<<"Enter Option = ";
		std::cin>>sw;
		switch(sw){
			case 1 :
				search<Book>(*(this),book,sw-1);
				break;
			case 2 :
				search<Mag>(*(this),mag,sw-1);
				break;
			case 3 :
				search<News_Paper>(*(this),news_paper,sw-1);
				break;
		}
	}
}
void Library::edit_library(){
	int sw2 = 1;
	while(sw2){
		int a = 0;
		int id;
		bool flagC;
		std::cout<<"1-Edit Book 2-Edit Mag 3-Edit News Paper 4-Edit Librarian 5-Edit Member 6-Edit Worker 7-Edit Writer  0-Exit;"<<std::endl<<"Enter Option = ";
		std::cin>>sw2;
		switch(sw2){
			case 1 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(book[a].id == id){
						std::cin>>book[a];
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 2 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(mag[a].id == id){
						std::cin>>mag[a];
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}				break;
			case 3 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(news_paper[a].id == id){
						std::cin>>news_paper[a];
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 4 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(librarian[a].idNumber == id){
						std::cin>>librarian[a];
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 5 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(member[a].idNumber == id){
						std::cin>>member[a];
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 6 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(worker[a].idNumber == id){
						std::cin>>worker[a];
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 7 :
				a=0;
				std::cout<<"Enter id "<<std::endl;
				std::cin>>id;
				flagC = false;
				while(a<num[sw2-1]){
					if(writer[a].idNumber == id){
						std::cin>>writer[a];
						flagC = true;
					}
					//std::cout << book[a];std::cout << "-----------------------------------";
					a=a+1;
				}
				if(flagC == false){std::cout<<"Not Found"<<std::endl;}
				break;
			case 0 :
				break;
		}
	}	
}
/*void Library::main_library(){
	int a=0,sw = 1;
	Library ab;
	while(sw){
		std::cout<<"1-Add 2-Show 3-Borrow;";
>>>>>>> 6d7b94c933f52f9613ece25bd3e8a6f2c54b45e0
		std::cin>>sw;
		switch(sw){
			case 1 :
				input_library();
				break;
			case 2:
				output_library();
				break;
			case 3:
<<<<<<< HEAD
				search_library();
				break;
=======
				borrow.main_borrow(*this);
>>>>>>> 6d7b94c933f52f9613ece25bd3e8a6f2c54b45e0
		}
	}
}*/
void Library::saveToFile(){
	std::vector<std::thread> threads;
	threads.push_back(std::thread(writeData<Book>,"books.bin",book,num[0]));
	threads.push_back(std::thread(writeData<Mag>,"mags.bin",mag,num[1]));
	threads.push_back(std::thread(writeData<News_Paper>,"newsPapers.bin",news_paper,num[2]));
	threads.push_back(std::thread(writeData<Librarian>,"librarians.bin",librarian,num[3]));
	threads.push_back(std::thread(writeData<Member>,"members.bin",member,num[4]));
	threads.push_back(std::thread(writeData<Worker>,"workers.bin",worker,num[5]));
	threads.push_back(std::thread(writeData<Writer>,"writers.bin",writer,num[6]));
	
	for(auto& thread : threads){
		thread.join();
	}
}

void Library::loadFromFile(){
	
	//loadData<Book>("books.bin",book,num[0]);
	std::vector<std::thread> threads;
	threads.push_back(std::thread([this](){
 			FileCls<Book> file("books.bin");
			file.readFromFile(this->num[0],&(this->book));	
 	}));
	threads.push_back(std::thread([this](){
 			FileCls<Mag> file("mags.bin");
			file.readFromFile(this->num[1],&(this->mag));	
	}));
	threads.push_back(std::thread([this](){
 			FileCls<News_Paper> file("newsPapers.bin");
			file.readFromFile(this->num[2],&(this->news_paper));	
	}));
	
	threads.push_back(std::thread([this](){
 			FileCls<Librarian> file("librarians.bin");
			file.readFromFile(this->num[3],&(this->librarian));	
	}));

	threads.push_back(std::thread([this](){
 			FileCls<Member> file("members.bin");
			file.readFromFile(this->num[4],&(this->member));	
	}));
	
	threads.push_back(std::thread([this](){
 			FileCls<Worker> file("workers.bin");
			file.readFromFile(this->num[5],&(this->worker));	
	}));
	
	threads.push_back(std::thread([this](){
 			FileCls<Writer> file("writers.bin");
			file.readFromFile(this->num[6],&(this->writer));	
	}));

	
	for(auto& thread : threads){
		thread.join();
	}
	
}
