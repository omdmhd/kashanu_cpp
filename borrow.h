#include "person.h"
#include "document.h"
#include "library.h"
#include "file.cpp"
#include <thread>
#include <fstream>
#include "exception.h"

template <typename T>
struct doc_brw{
	int employid;
	int memberid;
	T product;
};
class Borrow{
	doc_brw<News_Paper> *nps;
	int npb;
	doc_brw<Book> *books;
	int bookb;
	doc_brw<Mag> *mags;
    int magb;
    
	public:
	Borrow(){
		npb = 0;
		bookb = 0;
		magb = 0;
		nps = new doc_brw<News_Paper>[npb];
		books = new doc_brw<Book>[npb];
		mags = new doc_brw<Mag>[npb];
	}
	friend std::ostream &operator<<( std::ostream &output,const Borrow &D);

	void give_borrow(Library);
	void return_borrow(Library);
	void main_borrow(Library);
	void saveToFile();
	void loadFromFile();

};
