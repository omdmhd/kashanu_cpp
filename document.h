#include <iostream>
#include <string>
#include <cstring> 
#include "date.h"
#pragma once
//#include "date.cpp"

class Document{

	//protected:
	public:
	//protected:
		int id;
		char name[100];
		DateCls release_date;
		int price;
		int amount;
	
		virtual void add_document()=0;
		virtual void edit_document()=0;
};
class Book:public Document{
	private:
		int release_time;
		int writer_id;
	public:
		void add_document();
		void edit_document();
		friend std::ostream & operator<<(std::ostream &sh,  Book &c);
		friend std::istream & operator>>(std::istream &sh,  Book &c);
};
class Mag:public Document{
	private:
		int release_version;
		int company_id;
	public:
		void add_document();
		void edit_document();
		friend std::ostream & operator<<(std::ostream &sh,  Mag &c);
		friend std::istream & operator>>(std::istream &sh,  Mag &c);
};
class News_Paper:public Document{
	private:
		int company_id;
	public:
		void add_document();
		void edit_document();
		friend std::ostream & operator<<(std::ostream &sh,  News_Paper &c);
		friend std::istream & operator>>(std::istream &sh,  News_Paper &c);
};

		
