#include<iostream>
#pragma once
struct DateCls{	
	int day;
	int year;
	int month;
	void add_time();
	friend std::ostream & operator<<(std::ostream &sh, const DateCls &c);
	friend std::istream & operator>>(std::istream &sh,DateCls &c);
};
